﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripOrganization.Concrete.Base;

namespace TripOrganization.Interfaces
{
    public interface IManageBase
    {
        bool SaveTrip(TripReservation reservationParam);

        IEnumerable<TripReservation> GetAllTrips();
    }
}
