﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Patterns.Strategy
{
    public abstract class Calculation
    {
        /// <summary>
        /// koszt podróży danym środkiem lokomocji
        /// </summary>
        public decimal cost { get; set; }

        /// <summary>
        /// czas potrzebny na podróż w dwie strony w godznach
        /// </summary>
        public int timeInHour { get; set; }

        /// <summary>
        /// Oblicza koszta podróży
        /// </summary>
        /// <returns></returns>
        public abstract decimal CalculateCost();

        /// <summary>
        /// Oblicza czas podróży do miejsca docelowego
        /// </summary>
        /// <returns></returns>
        public abstract int CalculateTimeTrip();

        
    }
}