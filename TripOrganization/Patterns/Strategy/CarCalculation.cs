﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Patterns.Strategy
{
    public class CarCalculation : Calculation
    {
        private int _kilometers;
        private int _personCount;
        public CarCalculation(int kilometersParam,int personCountParam)
        {
            _kilometers = kilometersParam;
            _personCount = personCountParam;
        }

        public override decimal CalculateCost()
        {
            int carCount = _personCount/5;
            if ((_personCount - (carCount * 5)) != 0)
            {
                ++carCount;
            }

            cost = (_kilometers * 1.8m) * carCount;
            
            return cost;
        }

        public override int CalculateTimeTrip()
        {
            timeInHour = _kilometers / 60;
            if ((_kilometers - (timeInHour * 60)) != 0)
                ++timeInHour;
            return timeInHour;
        }
    }
}