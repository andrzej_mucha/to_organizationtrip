﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Patterns.Strategy
{
    public class PlaneCalculation : Calculation
    {
        private int _kilometers;
        private int _personCount;
        public PlaneCalculation(int kilometersParam, int personCountParam)
        {
            _kilometers = kilometersParam;
            _personCount = personCountParam;
        }

        public override decimal CalculateCost()
        {
            int planeCount = _personCount / 150;
            if ((_personCount - (planeCount * 150)) != 0)
            {
                ++planeCount;
            }

            cost = (_kilometers * 100m) * planeCount;

            return cost;
        }

        public override int CalculateTimeTrip()
        {
            timeInHour = _kilometers / 800;
            if ((_kilometers - (timeInHour * 800)) != 0)
                ++timeInHour;
            return timeInHour;
        }
    }
}