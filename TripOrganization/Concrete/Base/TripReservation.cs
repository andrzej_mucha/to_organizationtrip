//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TripOrganization.Concrete.Base
{
    using System;
    using System.Collections.Generic;
    
    public partial class TripReservation
    {
        public int Id { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string TripType { get; set; }
        public int PeopleCount { get; set; }
        public string TransportationType { get; set; }
        public decimal Price { get; set; }
        public string DateFromTo { get; set; }
        public string PlaceTrip { get; set; }
        public string Description { get; set; }
        public string PersonReservation { get; set; }
    }
}
