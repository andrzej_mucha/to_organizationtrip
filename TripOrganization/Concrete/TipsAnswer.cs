﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Models;

namespace TripOrganization.Concrete
{
    public class TipsAnswer
    {
        private Trip_type type_trip;
        private Transportation_type type_transport;
        public TipsAnswer(Trip_type typeTripParam, Transportation_type type_transportParam)
        {
            type_trip = typeTripParam;
            type_transport = type_transportParam;
        }

        public string GetTips(decimal priceParam)
        {
            if (priceParam > 1000 && type_trip == Trip_type.ABROAD)
            {
                return "Drogo ? Może warto wybrać wycieczkę w Polsce ?";
            }
            if (priceParam > 1000 && type_trip == Trip_type.COUNTRY&&type_transport!=Transportation_type.BIKE)
            {
                return "Drogo ? Może warto wybrać zdrowszą i tańszą odmianę środka transportu ? Rower ?";
            }
            return "Wygląda dobrze, POWODZENIA !";

        }


    }
}